9generator-terraform
=========

A role to bootstrap [Terraform](https://www.terraform.io/) projects using [9generator](https://gitlab.com/live9/ansible/playbooks/9generator). Currently the only task done is to generate the `terraform` block with the remote state configuration with the AWS s3 bucket needed.

Requirements
------------

Python 3.

Role Variables
--------------

This role depends on [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core) role, so it needs the following variables, plus any other needed by that role. This is the list of variables:

#### Mandatory

Global:

* project_type: It always must be _"terraform"_.
* environments: Defines the infrastructure environments where this service is going to be deployed. You need to specify at least one.

Per environment:

* bucket: The AWS s3 bucket name were the remote state is going to be stored.
* region: The AWS region were the bucket is.
* dynamodb_table: The name of the AWS DynamoDB table name were the remote state lock is going to be stored.

#### Optional

  Per environment:

  * aws_profile: The name of the AWS profile that has the access key and token were the infrastructure is located. If this is not set then the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` must be set.
  * terraform_modules_version: The terraform modules version to use (Default value: master).

Example `manifest.yml` file:

    ---
    # Mandatory variables
    project_type: terraform
    # Optional variables
    file_owner_id: 1000
    # Use this variable to set a different output directory for renderized templates
    # Default is ../
    out_dir: foo

    # Template dependent variables
    environments:
      prod:
        aws_profile: "acme"
        aws_region: "us-east-2"
        s3_bucket_name: "terraform-bucket-prod"
        dynamodb_table_name: "terraform-locks-prod"
        terraform_modules_version: "v1.0.0"
      staging:
        aws_profile: "acme2"
        aws_region: "us-east-1"
        s3_bucket_name: "terraform-bucket-staging"
        dynamodb_table_name: "terraform-locks-staging"
        terraform_modules_version: "v1.0.0"


Dependencies
------------

* [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core)

Example Playbook
----------------

    ---
      - name: Bootstrap project
        hosts: localhost
        connection: local
        gather_facts: no
        vars:
          manifest_path: "manifest.yml"
        tasks:
          - include_vars: "{{ manifest_path }}"
          - name: Bootstrap Terraform project
            include_role:
              name: 9generator-terraform

License
-------

GPLv3 or later

Author Information
------------------

Juan Luis Baptiste <juan.baptiste _at_ karisma.org.co >
