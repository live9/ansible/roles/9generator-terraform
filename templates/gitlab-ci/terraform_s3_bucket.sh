#!/bin/bash
# Bash script to create an AWS s3 bucket and DynamoDB table for terraform remote
# states storage using aws-cli.
#
# Author: Juan Luis Baptiste <juan.baptiste@karisma.org.co>
#

# defaults
DEBUG=${DEBUG:-0}
AWS_REGION="${AWS_REGION:-us-east-2}"
AWS_CLI=""
DELETE_BUCKET_TABLE=0
FAIL_ON_EXISTING_BUCKET="${FAIL_ON_EXISTING_BUCKET:-0}"
_create_bucket=1

function ctrl_c() {
  echo "Ctrl C pressed."
  exit 0
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

usage()
{
cat << EOF
Create an AWS s3 bucket.

This script will create or delete an s3 bucket and dynamodb table that can be used
to store terraform remote state. The script will check if there are a bucket and
table with the desired names and will fail if they already exists.

AWS API access configuration:

To configure the AWS API access this script can both use a profile name present
in a credentials file (ie. ~/.aws/credentials), or use the AWS_ACCESS_KEY_ID and
AWS_SECRET_ACCESS_KEY environment variables. The default region is "us-east-2"
but it can be overriden with the AWS_REGION environment variable.

Usage: $0 OPTIONS

OPTIONS:
-b    S3 bucket name (Mandatory)
-t    DynamoDB table name (Mandatory)
-D    Delete bucket and table (Optional)
-f    Path to AWS credentials file (Mandatory if profile name is set)
-p    AWS profile name (Mandatory if credentials file path is set)
-h    Print this help.
-V    Debug mode.

EOF
}

function init_aws_cmd(){
  if [[ "${AWS_PROFILE}" != "" && "${AWS_PROFILE_FILE}" != "" ]]; then
    echo -e "Using AWS Profile: ${AWS_PROFILE}\n"
    AWS_CLI="docker run --rm -v ${AWS_PROFILE_FILE}:/root/.aws/credentials -e AWS_DEFAULT_REGION=${AWS_REGION} -e AWS_PROFILE=${AWS_PROFILE} amazon/aws-cli "
  elif  [[ "${AWS_ACCESS_KEY_ID}" != "" && "${AWS_SECRET_ACCESS_KEY}" != "" ]]; then
    echo -e "Using AWS key: ${AWS_ACCESS_KEY_ID}\n"
    AWS_CLI="docker run --rm -e AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} -e AWS_DEFAULT_REGION=${AWS_REGION} -e AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} amazon/aws-cli "
  else
    echo "ERROR: AWS access configuration is not set. Please either set a profile name and credentials file location parameters or AWS_SECRET_ACCESS_KEY and AWS_ACCESS_KEY_ID environment variables first." && usage && exit 1
  fi
}

function aws_cmd() {
  local code=0

  if [ "${1}" != "" ]; then
    ${AWS_CLI} $1
    code=$?
  else
    echo "ERROR: Missing aws parameter." && exit 1
    code=1
  fi
  return ${code}
}

while getopts b:t:df:p:hV option
do
  case "${option}"
  in
    b) S3_BUCKET_NAME="${OPTARG}"
       ;;
    t) DYNAMODB_TABLE_NAME="${OPTARG}"
       ;;
    d) DELETE_BUCKET_TABLE=1
       ;;
    f) AWS_PROFILE_FILE="${OPTARG}"
       ;;
    p) AWS_PROFILE="${OPTARG}"
       ;;
    h) usage
       exit
       ;;
    V) DEBUG=1
       ;;
    ?) usage
       exit
       ;;
  esac
done

if [ ${DEBUG} -eq 1 ]; then
  set -x
fi

if [ $# -lt 1 ]; then
  usage
  exit 1
fi


if [ "${S3_BUCKET_NAME}" == "" ]; then
  echo "ERROR: s3 bucket name missing" && usage && exit 1
fi

if [ "${DYNAMODB_TABLE_NAME}" == "" ]; then
  echo "ERROR: DynamoDB table name missing" && usage && exit 1
fi

# Init aws cli command to use profile or keys
init_aws_cmd

if [ "${DELETE_BUCKET_TABLE}" -eq 0 ];then
    # Check first if bucket and table already exists before trying to create them
    echo "Validating there isn't already a bucket with the name ${S3_BUCKET_NAME} ..."
    out=$(aws_cmd "s3api list-buckets")
    echo ${out} | jq '.Buckets[].Name' | grep -i ${S3_BUCKET_NAME}  > /dev/null 2>&1
    [ $? -eq 0 ] && [ ${FAIL_ON_EXISTING_BUCKET} -eq 1 ] && echo -e "ERROR: A bucket with name '${S3_BUCKET_NAME}' already exists\n" && exit 1
    [ $? -eq 1 ] && _create_bucket=0 && echo -e "Bucket already exists, but allowed to continue\n"

    echo "Validating there isn't already a DynamoDB table with the name ${DYNAMODB_TABLE_NAME} ..."
    out=$(aws_cmd "dynamodb list-tables")
    echo ${out} | jq '.TableNames' | grep -oP '"\K[^",]+'|grep -w ${DYNAMODB_TABLE_NAME}  > /dev/null 2>&1
    [ $? -eq 0 ] && [ ${FAIL_ON_EXISTING_BUCKET} -eq 1 ] && echo -e "ERROR: A table with name '${DYNAMODB_TABLE_NAME}' already exists\n" && exit 1
    [ $? -eq 1 ] && CREATE_TABLE=0 && echo -e "Table already exists, but allowed to continue\n"

    # As they do not exist, create the bucket and the table
    if [ ${_create_bucket} -eq 1 ]; then
      echo "Creating bucket with name ${S3_BUCKET_NAME} in region ${AWS_REGION}..."
      out=$(aws_cmd "s3api create-bucket --bucket ${S3_BUCKET_NAME} \
                                   --create-bucket-configuration LocationConstraint=${AWS_REGION}")
      [ $? -gt 0 ] && echo -e "ERROR: Could not create bucket" && exit 1
      echo -e "Bucket created.\n"
    fi
    if [ ${CREATE_TABLE} -eq 1 ]; then
      echo "Creating DynamoDB table with name ${DYNAMODB_TABLE_NAME} in region ${AWS_REGION}..."
      out=$(aws_cmd "dynamodb create-table --table-name ${DYNAMODB_TABLE_NAME} \
                                     --attribute-definitions AttributeName=LockID,AttributeType=S \
                                     --key-schema AttributeName=LockID,KeyType=HASH \
                                     --billing-mode=PAY_PER_REQUEST \
                                     --region=${AWS_REGION}")
      [ $? -gt 0 ] && echo -e "ERROR: Could not create table" && exit 1
      echo -e "Table created.\n"
    fi
fi

if [ "${DELETE_BUCKET_TABLE}" -eq 1 ];then
  echo "Deleting bucket with name ${S3_BUCKET_NAME} ..."
  out=$(aws_cmd "s3api delete-bucket --bucket ${S3_BUCKET_NAME}")
  [ $? -gt 0 ] && echo -e "ERROR: Could not delete bucket" && exit 1
  echo -e "Bucket deleted.\n"

  echo "Deleting DynamoDB table with name ${DYNAMODB_TABLE_NAME} ..."
  out=$(aws_cmd "dynamodb delete-table --table-name ${DYNAMODB_TABLE_NAME}")
  [ $? -gt 0 ] && echo -e "ERROR: Could not delete table" && exit 1
  echo -e "Table deleted.\n"
fi

echo -e "Finished!\n"
