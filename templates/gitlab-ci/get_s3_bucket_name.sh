#!/bin/bash
# Script to extract the values of the s3 and dynamodb names from a manifest.yml
# file.
#
# It receives the path to the YAML file and the name of the environment from which
# we need to get the names of the bucket and the table, and exports the Variables
# S3_BUCKET_NAME and DYNAMODB_TABLE_NAME with the values found.
#
# Author: Juan Luis Baptiste <juan.baptiste@karisma.org.co>
#

# shellcheck disable=SC2154
# shellcheck disable=SC1091

DEBUG="${DEBUG:-0}"
[ $DEBUG -eq 1 ] && set -x && env

set -Eeo pipefail
_traperr() {
  echo "ERROR: ${BASH_SOURCE[1]} at about line ${BASH_LINENO[0]}"
}
trap _traperr ERR

# YAML processing script taken from:
# https://github.com/jasperes/bash-yaml
# shellcheck source=${PWD}
source ${PWD}/gitlab-ci/yaml.sh

yaml_file=${1}
env=${2}
[ -z ${yaml_file} ] && echo "ERROR: Did not pass yaml file for processing." && exit 1
[ -z ${env} ] && echo "ERROR: Did not pass environment name to look for." && exit 1

# Load YAML file contents into variables
create_variables ${yaml_file}

# Define the variable names we need to get the values from
s3_bucket_var_name="environments_${env}_s3_bucket_name"
dynamodb_table_var_name="environments_${env}_dynamodb_table_name"
# Extract the previous variables values
S3_BUCKET_NAME=${!s3_bucket_var_name}
DYNAMODB_TABLE_NAME=${!dynamodb_table_var_name}

# Fail we could not find the values
[ -z ${S3_BUCKET_NAME} ] && echo "ERROR: Did not find S3 bucket name !" && exit 1
[ -z ${DYNAMODB_TABLE_NAME} ] && echo "ERROR: Did not find DynamoDB table name !" && exit 1

echo "s3 bucket: ${S3_BUCKET_NAME}"
echo "DynamoDB table name: ${DYNAMODB_TABLE_NAME}"

# Export values
echo S3_BUCKET_NAME=${S3_BUCKET_NAME} > .s3_env
echo DYNAMODB_TABLE_NAME=${DYNAMODB_TABLE_NAME} >> .s3_env
